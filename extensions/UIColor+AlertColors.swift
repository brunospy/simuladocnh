//
//  UIColor+AlertColors.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 10/15/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

extension UIColor {

    class func greenAlertColor() -> UIColor{
        return UIColor(red: 0/255, green: 102/255, blue: 51/255, alpha: 1);
    }
    
    class func yellowAlertColor() -> UIColor{
        return UIColor(red: 192/255, green: 156/255, blue: 38/255, alpha: 1);
    }
    
    class func redAlertColor() -> UIColor {
        return UIColor(red:192.0/255.0, green:156.0/255.0, blue:38.0/255.0, alpha:1.0);
    }
    
}
