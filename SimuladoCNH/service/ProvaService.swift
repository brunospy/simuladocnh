//
//  ProvaService.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/17/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import Foundation

class ProvaService: NSObject {
    
    class func todasAsProvas() -> Array<Prova>{
        let file = "provas"
        let path = Bundle.main.path(forResource: file, ofType: "xml")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!))
        return parserXMLSAX(data!)
    }
    
    class func provas() -> Prova{
        //Cria prova
        let prova = Prova()
        prova.nome = "Meio ambiente"
        prova.quantidadeQuestoes = 37
        
        let questao = Questao()
        questao.titulo = "A poluição do ar causa problemas de saúde que resultam, principalmente em:"
        
        let respostaA = Resposta()
        respostaA.resposta = "Doenças do aparalho digestivo."
        
        let respostaB = Resposta()
        respostaB.resposta = "Doenças respiratórias."
        
        let respostaC = Resposta()
        respostaC.resposta = "Alterações visuais."
        
        let respostaD = Resposta()
        respostaD.resposta = "Dores de cabeça."
        
        let respostaE = Resposta()
        respostaE.resposta = "Doenças do aparelho locomotor."
        
        let questao2 = Questao()
        questao2.titulo = "Diante da placa R-6, você entende que:"
        
        
        let respostaA2 = Resposta()
        respostaA2.resposta = "Pode estacionar seu veículo no trecho regulamentado."
        
        let respostaB2 = Resposta()
        respostaB2.resposta = "É proibida a circulação de veículo especiais."
        
        let respostaC2 = Resposta()
        respostaC2.resposta = "É proibido estacionar no trecho abrangido pela restrição, podendo correr paradas breves."
        
        let respostaD2 = Resposta()
        respostaD2.resposta = "É proibida a circulação de pedestres na via ou área sinalizada"
        
        let respostaE2 = Resposta()
        respostaE2.resposta = "É proibido parar e estacionar, ainda que para operação de embarque e desembarque por curto espaço de tempo."

        questao2.respostas = [respostaA2, respostaB2, respostaC2, respostaD2, respostaE2]
        questao2.respostaCorreta = 2
        questao.respostas = [respostaA, respostaB, respostaC, respostaD, respostaE]
        questao.respostaCorreta = 1
        
        prova.questoes = [questao, questao2]
        return prova
    }
    
    class func parserXMLSAX(_ data:Data) -> Array<Prova>{
        if (data.count == 0){
            return []
        }
        
        var provas: Array<Prova> = []
        let xmlParser = XMLParser(data:data)
        let provaParser = XMLParserProva()
        
        xmlParser.delegate = provaParser
        let ok = xmlParser.parse()
        
        if (ok){
            provas = provaParser.provas
        }
        return provas
    }
    
    class func getProvas() -> Array<Prova> {
        var provas:Array<Prova> = []
        let file = "provas"
        let path = Bundle.main.path(forResource: file, ofType: "json")
        let data = try? Data(contentsOf: URL(fileURLWithPath: path!))

        let dictionary = try? JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
    
        if let dict = dictionary {
            let arrayProvas = dict["provas"] as! NSMutableArray
            let count = arrayProvas.count - 1
            
            for index in 0...count{
                let provaDictionary = arrayProvas[index] as! NSDictionary
                print(provaDictionary)
                let prova = Prova()
                prova.id = provaDictionary["id"] as! Int
                prova.nome = provaDictionary["nome"] as! String
                prova.nivel = provaDictionary["dificuldade"] as! String
                prova.quantidadeQuestoes = provaDictionary["numeroQuestoes"] as! Int
                provas.append(prova)
            }
        }
        return provas
    }
}
