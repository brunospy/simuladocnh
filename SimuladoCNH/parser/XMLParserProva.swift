//
//  XMLParserProva.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/17/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import Foundation

class XMLParserProva: NSObject, XMLParserDelegate {
    var provas:Array<Prova> = []
    var prova:Prova?
    var valor:String?
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        
        if ("prova" == elementName){
            prova = Prova()
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if ("provas" == elementName){
            return
        }
        
        if ("prova" == elementName){
            prova?.nome = valor
            provas.append(prova!)
            prova = nil
            return
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        valor = string
    }
}
