//
//  Questao.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/13/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import Foundation

class Questao{
    
    var titulo:String!
    var resposta:Int!
    var respostaCorreta:Int!
    var respostas:Array<Resposta>!
    
}
