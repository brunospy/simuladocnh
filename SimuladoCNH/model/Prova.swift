//
//  Prova.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/13/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import Foundation

class Prova {

    var id:Int!
    var nome:String!
    var nivel:String!
    var quantidadeQuestoes:Int!
    var questoes:Array<Questao> = []
    
}