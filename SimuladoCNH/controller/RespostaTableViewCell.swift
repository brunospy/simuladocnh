//
//  RespostaTableViewCell.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/26/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

class RespostaTableViewCell: UITableViewCell {

   
    @IBOutlet weak var imagem: UIImageView!
    @IBOutlet weak var resposta: UILabel!
}
