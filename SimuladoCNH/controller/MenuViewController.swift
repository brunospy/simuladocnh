//
//  MenuViewController.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 10/24/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{


    @IBOutlet var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 3;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMenu")! as UITableViewCell
        return cell;
    }
    

}

