//
//  ProvaViewController.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/13/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit
import FCAlertView


class ProvaViewController: UIViewController, UINavigationBarDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var prova:Prova!
    
    var imagens:Array<String> = ["1.ico", "2.ico", "3.ico", "4.ico"]
    
    var questao:Questao!

    @IBOutlet weak var correta: UILabel!
    @IBOutlet weak var Pergunta: UILabel!
    @IBOutlet weak var errada: UILabel!
    
    var respostasCorretas:Int = 0
    
    var respostasErradas:Int = 0
    
    var total = 0
    
    var questaoAtual = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        self.prova = ProvaService.provas()
        self.questao = prova.questoes[0]
        self.total = prova.quantidadeQuestoes
        self.Pergunta.text = questao.titulo
        
        let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        navigationBar.delegate = self;
        
        // Create a navigation item with a title
        let navigationItem = UINavigationItem()
        navigationItem.title = "Prova - simulação"
        navigationBar.items = [navigationItem]
        
        let backgroundColor = UIColor(red:0.0/255.0, green:102.0/255.0, blue:51.0/255.0, alpha:0.0)
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]

        UINavigationBar.appearance().barTintColor = backgroundColor
        
        let leftButton =  UIBarButtonItem(title: "voltar", style:   UIBarButtonItemStyle.plain, target: self, action: #selector(ProvaViewController.back(_:)))
        
        navigationItem.leftBarButtonItem = leftButton
        self.view.addSubview(navigationBar)
    }
   
    func back(_ sender: UIBarButtonItem) {
        let image:UIImage = UIImage(named: "warning.png")!
        let alert = FCAlertView()
        alert.delegate = self
        alert.colorScheme = UIColor.yellowAlertColor()
        alert.showAlert(inView: self, withTitle: "Vai desistir da prova?", withSubtitle:nil, withCustomImage: image, withDoneButtonTitle: "Desistir", andButtons: ["Cancelar"]);
    }
    
    func desistir(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SimulacaoViewController") as! SimulacaoViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "celulaResposta")! as! RespostaTableViewCell
        cell.imagem.image = UIImage(named: imagens[(indexPath as NSIndexPath).row])
        
        let resposta = questao.respostas[(indexPath as NSIndexPath).row]
        cell.resposta.text = resposta.resposta
        return cell;
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let resposta = questao.respostaCorreta
        if resposta == (indexPath as NSIndexPath).row {
            respostasCorretas = respostasCorretas + 1
            let x = respostasCorretas as NSNumber
            self.correta.text = x.stringValue
           
            let alert = FCAlertView()
            alert.colorScheme = UIColor.greenAlertColor()
            alert.showAlert(inView: self, withTitle: "Resposta correta!", withSubtitle:"Parabéns :)", withCustomImage: UIImage(named: "checked.png"), withDoneButtonTitle: "OK", andButtons: nil);
        } else {
            respostasErradas = respostasErradas + 1
            let x = respostasErradas as NSNumber
            self.errada.text = x.stringValue

            let alert = FCAlertView()
            alert.colorScheme = UIColor.redAlertColor();
            alert.showAlert(inView: self, withTitle: "Resposta errada!", withSubtitle:"Você precisa praticar mais :(", withCustomImage: UIImage(named: "checked.png")!, withDoneButtonTitle: "OK", andButtons: nil);
        }
        
        proximaQuestao()
    }

    func proximaQuestao(){
        self.questaoAtual = self.questaoAtual + 1
        self.Pergunta.text = ""
        self.questao = self.prova.questoes[self.questaoAtual]
        self.Pergunta.text = self.questao.titulo
        self.tableView.reloadData()
    }
}

extension ProvaViewController: FCAlertViewDelegate{
    func fcAlertDoneButtonClicked(_ alertView: FCAlertView!){
       desistir()
    }
}
