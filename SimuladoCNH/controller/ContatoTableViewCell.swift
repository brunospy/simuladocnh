//
//  ContatoTableViewCell.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/25/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

class ContatoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var Telefone: UILabel!
    @IBOutlet weak var nome: UILabel!
    @IBOutlet weak var endereco: UILabel!
}
