//
//  CelulaTipoProvaTableViewCell.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/13/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

class CelulaTipoProvaTableViewCell: UITableViewCell {

    @IBOutlet weak var titulo: UILabel!
    @IBOutlet weak var nivel: UILabel!
}
