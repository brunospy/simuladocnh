//
//  SimuladoCNHViewController.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/19/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

class SimuladoCNHViewController: UIViewController, UINavigationBarDelegate {
   
    var menuButton = UIBarButtonItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        navigationBar.delegate = self;
        
        // Create a navigation item with a title
        let navigationItem = UINavigationItem()
        navigationItem.title = "Prova - simulação"
        navigationBar.items = [navigationItem]
        
        let backgroundColor = UIColor(red:0.0/255.0, green:102.0/255.0, blue:51.0/255.0, alpha:0.0)
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        UINavigationBar.appearance().barTintColor = backgroundColor
        
        let menuButton =  UIBarButtonItem(title: "Menu", style:   UIBarButtonItemStyle.plain, target: self, action: nil)
        
        navigationItem.leftBarButtonItem = menuButton
        self.view.addSubview(navigationBar)

        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }
    
    
}
