//
//  UnidadesViewController.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/25/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit

class UnidadesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UINavigationBarDelegate {
    
    var unidades:Array<String> = ["Nova Geração - Jardim Angela", "Nova Geração - Embu", "Nova Geração - Pirajussara", "Nova Geração - Carapicuiba"]
    var enderecos:Array<String> = [" 04948-030, Estrada do M'Boi Mirim, 3646 - Jardim Angela, São Paulo - State of São Paulo, 04905-003","Estr. São José, 1237 - Jardim Marina, Embu das Artes - SP, 06826-315", "Av. Aimara, 869 - Parque Pirajussara, Embu das Artes - SP, 06815-000", "Estr. Acácias, 1761 - Parque Roseira, Carapicuíba - SP, 06385-023"]

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        navigationBar.delegate = self;
        
        // Create a navigation item with a title
        let navigationItem = UINavigationItem()
        navigationItem.title = "Unidades do Detran"
        navigationBar.items = [navigationItem]
        
        let backgroundColor = UIColor(red:0.0/255.0, green:102.0/255.0, blue:51.0/255.0, alpha:0.0)
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        UINavigationBar.appearance().barTintColor = backgroundColor
        
        let leftButton =  UIBarButtonItem(title: "voltar", style:   UIBarButtonItemStyle.plain, target: self, action: #selector(SimulacaoViewController.back(_:)))
        
        navigationItem.leftBarButtonItem = leftButton
        
        self.view.addSubview(navigationBar)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func back(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "reveal") as! SWRevealViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
        vc.enderecoText = enderecos[(indexPath as NSIndexPath).row]
        vc.nomeText = unidades[(indexPath as NSIndexPath).row]
        self.present(vc, animated: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return unidades.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "celUnidade")! as! ContatoTableViewCell
        let linha = (indexPath as NSIndexPath).row
        cell.nome.text = unidades[linha]
        cell.endereco.text = enderecos[linha]
       
        return cell;
    }
}
