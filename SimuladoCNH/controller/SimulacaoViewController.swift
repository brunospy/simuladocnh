//
//  SimulacaoViewController.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/13/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit
import FCAlertView


class SimulacaoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UINavigationBarDelegate{

    var provas:Array<Prova> = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.provas = ProvaService.getProvas()
        
        let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        navigationBar.delegate = self;
        
        // Create a navigation item with a title
        let navigationItem = UINavigationItem()
        navigationItem.title = "Provas"
        navigationBar.items = [navigationItem]
        
        let backgroundColor = UIColor(red:0.0/255.0, green:102.0/255.0, blue:51.0/255.0, alpha:0.0)
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        UINavigationBar.appearance().barTintColor = backgroundColor
        
        let leftButton =  UIBarButtonItem(title: "voltar", style:   UIBarButtonItemStyle.plain, target: self, action: #selector(SimulacaoViewController.back(_:)))
        
        navigationItem.leftBarButtonItem = leftButton
        
        self.view.addSubview(navigationBar)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    func back(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SimuladoCNHViewController") as! SimuladoCNHViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return provas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as! CelulaTipoProvaTableViewCell
        let linha = (indexPath as NSIndexPath).row
        let prova = provas[linha]
    
        cell.titulo.text = prova.nome
        cell.nivel.text = prova.nivel
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let prova = provas[(indexPath as NSIndexPath).row]
        let questoes = prova.quantidadeQuestoes as NSNumber
        let titulo:String = "Esta prova contém " + questoes.stringValue + " questões"
        
        let alert = FCAlertView()
        alert.delegate = self
        alert.colorScheme = UIColor.greenAlertColor()
        alert.showAlert(inView: self, withTitle: titulo, withSubtitle: "Deseja prosseguir?", withCustomImage: nil, withDoneButtonTitle: "Continuar", andButtons: ["Cancelar"]);
    }
    
   
    func irParaProva(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ProvaViewController") as! ProvaViewController
        self.present(vc, animated: true, completion: nil)
    }

}

extension SimulacaoViewController:FCAlertViewDelegate{

    func fcAlertDoneButtonClicked(_ alertView: FCAlertView!){
        irParaProva()
    }

}
