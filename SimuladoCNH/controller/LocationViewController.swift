//
//  LocationViewController.swift
//  SimuladoCNH
//
//  Created by Bruno Ribeiro on 9/26/16.
//  Copyright © 2016 Bruno Ribeiro. All rights reserved.
//

import UIKit
import MapKit

class LocationViewController: UIViewController, UINavigationBarDelegate{

    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var endereco: UILabel!
    
    var enderecoText:String = ""
    var nomeText:String = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()

        self.endereco.text = enderecoText
        
        let navigationBar = UINavigationBar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50))
        navigationBar.delegate = self;
        
        // Create a navigation item with a title
        let navigationItem = UINavigationItem()
        navigationItem.title = nomeText
        navigationBar.items = [navigationItem]
        
        let backgroundColor = UIColor(red:0.0/255.0, green:102.0/255.0, blue:51.0/255.0, alpha:0.0)
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        
        UINavigationBar.appearance().barTintColor = backgroundColor
        
        let leftButton =  UIBarButtonItem(title: "voltar", style:   UIBarButtonItemStyle.plain, target: self, action: #selector(SimulacaoViewController.back(_:)))
        
        navigationItem.leftBarButtonItem = leftButton
        
        self.view.addSubview(navigationBar)
        self.tabBarController?.tabBar.isHidden = true

    }
    
    func back(_ sender: UIBarButtonItem) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "UnidadeViewController") as! UnidadesViewController
        self.present(vc, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
